

$(document).ready(function() {


  // Blank default searchaddress text on click 
  $('#edit-searchaddress').click(function () {
    $(this).attr('value', '');
    $(this).unbind('click');
  }); 
  $('#edit-urhere').click(function () {
    $(this).attr('value', '');
    $(this).unbind('click');
  }); 

  // get gmap created map object for use with google maps api
  initialise();

  // Capture the TravellingFrom form's submit
  $('form#travellingfrom-form').submit(function() {
    storelocatorUserLoc = ($('input#edit-urhere').val())+', '+($('input#edit-country').val());

    // Remove previous URHere icon and update directions
    map.removeOverlay(dirMarkers[0]);
    setDirections(storelocatorUserLoc, storelocatorStoreLoc, storelocatorLocale);

    return false;
  });

});


// unload to reduce browser memory leaks as per 
// http://code.google.com/apis/maps/documentation/index.html#Memory_Leaks
$(window).unload( function () {
  GUnload();
})


function initialise() {
  // get gmap created map object for use with google maps api
  if (GBrowserIsCompatible()) {
    var m = Drupal.gmap.getMap('storelocator_map');
    if (m.map) {
      initDirection();
    }
    else {
      m.bind('ready', function(data) {
        initDirection();
      });
    }

  }

  initURHereIcon();

}


function initURHereIcon() {

  if ( typeof Drupal.gmap.icons == "undefined" ) { 
    // Keep calling self until Drupal.gmap.icons is defined 
    setTimeout('initURHereIcon()', 1000);
  }
  else {
    // Drupal.gmap.icons is defined, set up custom URHere Icon
    urhereObj = Drupal.gmap.getIcon(Drupal.settings.urhere_marker,0);

    // set up custom urhere icon
    urhIcon = new GIcon(G_DEFAULT_ICON);
    urhIcon.iconSize = new GSize(urhereObj.iconSize.width, urhereObj.iconSize.height);
    urhIcon.iconAnchor = new GPoint(urhereObj.iconAnchor.x, urhereObj.iconAnchor.y);
    urhIcon.shadowSize = new GSize(0, 0); // no shadow

    dirMarkers = [];
    latLngs = [];

    custIcons = [];
    custIcons[0] = new GIcon(urhIcon);
    custIcons[0].image = urhereObj.image;
   }
}


function initDirection() {
    map = Drupal.gmap.getMap('storelocator_map').map;
    gdir = new GDirections(map);
    GEvent.addListener(gdir, "load", onGDirectionsLoad);
    GEvent.addListener(gdir, "addoverlay", onGDirectionsAddOverlay)
    GEvent.addListener(gdir, "error", handleErrors);
}


function updateStore(storeLocation, userLocation, storeID, locale) {
  storelocatorStoreLoc = storeLocation;
  storelocatorLocale = locale;
  toStoreID = storeID;
  
  // Only set storelocatorUserLoc if it isn't already defined 
  if ( typeof storelocatorUserLoc == "undefined" ) {
    storelocatorUserLoc = userLocation;
  }

  // Add store HTML to Destination Macs div
  // FIX - check for existence of block before trying to update
  document.getElementById('storelocator-destination-store').innerHTML = document.getElementById(storeID).innerHTML;
  // Remove the directions link
  $("#storelocator-destination-store div.map-list-directions").remove();
  // Update directions
  setDirections(storelocatorUserLoc, storelocatorStoreLoc, storelocatorLocale);
}


function setDirections(fromAddress, toAddress, locale) {
    gdir.load("from: " + fromAddress + " to: " + toAddress,
            { "locale": locale, getSteps: true });
    // Make div visible
    document.getElementById('block-storelocator-3').style.display = 'inline';
    document.getElementById('block-storelocator-4').style.display = 'inline';
}


function onGDirectionsLoad(){
  // Customise the directions output
  //console.log(gdir.getRoute(0));
  var route = gdir.getRoute(0);
  var startLocality = route.cu.address;
  var endLocality = route.Wp.address;
  var bounds = new GLatLngBounds();
  var newHTML;
  var rowClass = 'odd';
  var storeMarker = $('#'+toStoreID+' .map-list-marker').html();

  newHTML = '<table class="storelocator-directions"><tr class="table-wrap head"><td class="storelocator-directions-start">'
    + '<img src="' + urhereObj.image + '" /></td>'
    + '<td class="corner-right">' + startLocality + '</td></tr>';

  for(i=0;i< route.getNumSteps();i++) {
    var curStep = route.getStep(i);
    newHTML = newHTML + '<tr class="' + rowClass + '"><td class="number">' + (i+1)
      + '.</td><td class="instruction">' + curStep.getDescriptionHtml() + '</td></tr>';
    if(rowClass=='odd') {
      rowClass='even';
    }
    else {
      rowClass='odd';
    }
  }

  newHTML = newHTML + '<tr class="table-wrap foot"><td class="storelocator-directions-end">'
    + storeMarker + '</td><td class="corner-right">'
    + endLocality + '</td></tr></table>';

  document.getElementById('storelocator-directions').innerHTML = newHTML;

}


function onGDirectionsAddOverlay() {
  // Replace default markers with our own 
  for (var i=0; i<=gdir.getNumRoutes(); i++){
    var originalMarker = gdir.getMarker(i);
    latLngs[i] = originalMarker.getLatLng();
    map.removeOverlay(originalMarker);
    if(i==0) {
      dirMarkers[i] = new GMarker(latLngs[i],{icon:custIcons[i]});
      map.addOverlay(dirMarkers[i]);
    }
  }
  // Zoom map out one level so that northern markers are always visible
  map.zoomOut();

}


function handleErrors(){
  if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
    alert("No corresponding geographic location could be found for one of the specified addresses. This may be due to the fact that the address is relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);

  else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
    alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);

  else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
    alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code);

  else if (gdir.getStatus().code == G_GEO_BAD_KEY)
    alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);

  else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
    alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);

  else alert("An unknown error occurred.");

}
